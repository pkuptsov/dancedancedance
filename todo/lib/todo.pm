package todo;
use Dancer ':syntax';

our $VERSION = '0.1';

get '/' => sub {
    template 'index';
};

# https://metacpan.org/pod/Text::Xslate

true;
